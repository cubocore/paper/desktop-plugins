/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QBasicTimer>
#include <paperprime/cplugininterface.h>

QT_BEGIN_NAMESPACE
namespace Ui {
    class Rotation;
}
QT_END_NAMESPACE

class Rotation : public QWidget {
    Q_OBJECT

public:
    Rotation(QWidget *parent = nullptr);
    ~Rotation();

protected:
    void timerEvent(QTimerEvent *tEvent);

private slots:
    void check_status();
    void init();
    void on_toolButton_autorotate_clicked(bool checked);
    void on_toolButton_normal_clicked();
    void on_toolButton_left_clicked();
    void on_toolButton_right_clicked();
    void on_toolButton_invert_clicked();

private:
    Ui::Rotation *ui;
    QBasicTimer *timer;
};

class RotationPlugin : public plugininterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID COREACTION_PLUGININTERFACE)
    Q_INTERFACES(plugininterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};
