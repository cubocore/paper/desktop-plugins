/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QProcess>

#include "rotation.h"
#include "ui_rotation.h"

Rotation::Rotation(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Rotation),
      timer(new QBasicTimer)
{
    ui->setupUi(this);
    init();
}

Rotation::~Rotation()
{
    delete ui;
}

void Rotation::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == timer->timerId()) {
        check_status();
    }
}

void Rotation::init()
{
    // 1 second timer
    timer->start(1000, this);

    // initialization
    check_status();
}

void Rotation::check_status()
{
    QProcess proc;

    //rotation status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-rotation.sh");
    proc.waitForFinished();

    QString rot = proc.readAllStandardOutput();
    rot = rot.trimmed();

    //qDebug()<< rot;
    if (rot == "normal") {
        ui->toolButton_normal->setChecked(true);
    } else {
        ui->toolButton_normal->setChecked(false);
    }

    if (rot == "left") {
        ui->toolButton_left->setChecked(true);
    } else {
        ui->toolButton_left->setChecked(false);
    }

    if (rot == "right") {
        ui->toolButton_right->setChecked(true);
    } else {
        ui->toolButton_right->setChecked(false);
    }

    if (rot == "invert") {
        ui->toolButton_invert->setChecked(true);
    } else {
        ui->toolButton_invert->setChecked(false);
    }
}

void Rotation::on_toolButton_autorotate_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/autorotate-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/autorotate-off.sh");
        proc.waitForFinished();
    }
}


void Rotation::on_toolButton_normal_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/rotate-normal.sh");
    proc.waitForFinished();
}

void Rotation::on_toolButton_left_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/rotate-left.sh");
    proc.waitForFinished();
}

void Rotation::on_toolButton_right_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/rotate-right.sh");
    proc.waitForFinished();
}

void Rotation::on_toolButton_invert_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/rotate-invert.sh");
    proc.waitForFinished();
}

/* Name of the plugin */
QString RotationPlugin::name()
{
    return "Rotation";
}

/* The plugin version */
QString RotationPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *RotationPlugin::widget(QWidget *parent)
{
    return new Rotation(parent);
}
