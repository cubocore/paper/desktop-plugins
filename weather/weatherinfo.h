/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QObject>
#include <QMap>
#include <QDateTime>
#include <QXmlStreamReader>

#include "weatherdata.h"

typedef QVector<WeatherData> WeatherForecasts;
typedef QMap<QDate, WeatherForecasts> DailyForecasts;
typedef QMap<QDateTime, WeatherForecasts> HourlyForecasts;

class weatherInfo : public QObject {
    Q_OBJECT
public:
    explicit weatherInfo(QString filepath, QObject *parent = nullptr);

    DailyForecasts dailyForecasts();
    HourlyForecasts hourlyForecasts();
    void initializeParse();

private:
    QString m_filepath;
    DailyForecasts m_df;
    HourlyForecasts m_hf;

    void parseData(QXmlStreamReader &reader);
    WeatherData parseLocation(QXmlStreamReader &reader);
    void mergeForecasts(WeatherForecasts &forecasts, bool daily);
};
