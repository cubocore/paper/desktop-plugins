/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QNetworkReply>
#include <QNetworkConfigurationManager>

#include "apirequest.h"

apiRequest::apiRequest(QObject *parent) :
    QObject(parent)
{
    qDebug() << "Initializing network session...";
    QNetworkConfigurationManager ncm;
    nam = new QNetworkAccessManager(this);
    ns = new QNetworkSession(ncm.defaultConfiguration(), this);

    ns->open();
    qDebug() << "Session opened: " << ns->isOpen();

//    connect(ns, &QNetworkSession::opened, this, &apiRequest::getRequestedData);
}

bool apiRequest::requestData(QString url, QString filepath, Reason r)
{
    if (!ns->isOpen()) {
        return false;
    }

    QNetworkReply *reply = nam->get(QNetworkRequest(QUrl(url)));
    connect(reply, &QNetworkReply::finished, [this, filepath, r, reply]() {
        qDebug() << "Saving requested data...";
        QFile file(filepath);
        qDebug() << "Save file path: " << file.fileName();
        qDebug() << "File opened: " << file.open(QIODevice::WriteOnly | QIODevice::Truncate);
        QTextStream in(&file);
        in << reply->readAll();
        file.close();
        emit fileDownloaded(r);
    });

    return ns->isOpen();
}
