/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include "search.h"
#include "ui_search.h"

#include "QDir"

#include <paperprime/appopenfunc.h>
#include <libpaperdesktop/papersettings.h>


search::search(QWidget *parent) :QWidget(parent),ui(new Ui::search)
{
    ui->setupUi(this);
    // settingsManage *sm = settingsManage::initialize();
    // QString temp = sm->value("CoreApps", "SearchApp");
    // ui->searchApp->setText(temp.count() ? temp : tr("Set Search app in CoreGrage"));

    ui->keyword->setInputMask(tr("Search by typing here"));

    connect( ui->keyword, SIGNAL( returnPressed() ), this, SLOT( on_startSearch_clicked() ) );
}

search::~search()
{
    delete ui;
}

void search::on_startSearch_clicked()
{
	PaperPrime::AppOpenFunc::defaultAppEngine(PaperPrime::Category::SearchApp, QDir::homePath(), ui->keyword->text(), this);
}

/* Name of the plugin */
QString searchPlugin::name()
{
    return "Search";
}

/* The plugin version */
QString searchPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *searchPlugin::widget(QWidget *parent)
{
    return new search(parent);
}
