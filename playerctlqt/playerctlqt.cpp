/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QTimer>
#include <QProcess>
#include <QDebug>
#include <QPixmap>
#include <QTime>

#include "playerctlqt.h"
#include "ui_playerctlqt.h"

playerctlqt::playerctlqt(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::playerctlqt),
      timer(new QBasicTimer)
{
    ui->setupUi(this);
    init();
}

playerctlqt::~playerctlqt()
{
    delete ui;
}
void playerctlqt::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == timer->timerId()) {
        get_playing_media();
    }
}

void playerctlqt::init()
{
    // connections
//    connect(timer, &QTimer::timeout, this, &playerctlqt::get_playing_media);
//    connect(timer, &QTimer::timeout, this, &playerctlqt::check_status);

    // 1 second timer
    timer->start(1000, this);

    // initialization
    get_playing_media();
}

void playerctlqt::get_playing_media()
{
    QProcess proc;
    // playerctl
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/playerctl-metadata.sh");
    proc.waitForFinished();
    QString t = proc.readAllStandardOutput();
    proc.start("playerctl", QStringList() << "status");
    proc.waitForFinished();
    QString s = proc.readAllStandardOutput();
    s = s.trimmed();
    ui->title->setText(t);

    if (s == "Playing") {
        ui->toolButton_playpause->setText("Pause");
        ui->toolButton_playpause->setIcon(QIcon::fromTheme("media-playback-pause"));
        ui->toolButton_playpause->setChecked(true);
        ui->toolButton_stop->setChecked(false);
    } else if (s == "Paused") {
        ui->toolButton_playpause->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->toolButton_playpause->setChecked(true);
        ui->toolButton_playpause->setText("Play");
        ui->toolButton_stop->setChecked(false);
    } else if (s == "Stopped") {
        ui->toolButton_playpause->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->toolButton_stop->setChecked(true);
        ui->toolButton_playpause->setChecked(false);
        ui->toolButton_playpause->setText("Play");
    } else {
        ui->toolButton_playpause->setIcon(QIcon::fromTheme("media-playback-start"));
        ui->toolButton_playpause->setChecked(false);
        ui->toolButton_stop->setChecked(false);
        ui->toolButton_playpause->setText("Play");
    }

    //get albumart
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/albumart.sh");
    proc.waitForFinished();
    QString album = proc.readAllStandardOutput();
    album = album.trimmed();
    //qDebug()<< album;

    //QString url = R"(album)";
    QPixmap img(album);    
    ui->albumart->setPixmap(img);
}

void playerctlqt::on_toolButton_prev_clicked()
{
    QProcess proc;
    proc.startDetached("playerctl", QStringList() << "previous");
}

void playerctlqt::on_toolButton_playpause_clicked()
{
    QProcess proc;
    proc.startDetached("playerctl", QStringList() << "play-pause");
}

void playerctlqt::on_toolButton_next_clicked()
{
    QProcess proc;
    proc.startDetached("playerctl", QStringList() << "next");
}

void playerctlqt::on_toolButton_shuffle_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("playerctl", QStringList() << "shuffle" << "on");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("playerctl", QStringList() << "shuffle" << "off");
        proc.waitForFinished();
    }
}


void playerctlqt::on_toolButton_stop_clicked()
{
    QProcess proc;
    proc.startDetached("playerctl", QStringList() << "stop");
    proc.waitForFinished();
}

/* Name of the plugin */
QString playerctlqtPlugin::name()
{
    return "PlayerctlQt";
}

/* The plugin version */
QString playerctlqtPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *playerctlqtPlugin::widget(QWidget *parent)
{
    return new playerctlqt(parent);
}
