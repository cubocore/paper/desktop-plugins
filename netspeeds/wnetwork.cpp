/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QTimer>

#include <paperprime/filefunc.h>
#include <csys/info_manager.h>

#include "wnetwork.h"
#include "ui_wnetwork.h"


wNetwork::wNetwork(QWidget *parent) : QWidget(parent), ui(new Ui::wNetwork),
    timer(new QBasicTimer()),
    im(InfoManager::ins())
{
    ui->setupUi(this);

    checkNetwork();
    usageFile = new QSettings("paperdesktop", "networkusage");
    currDT = QDateTime::currentDateTime();

    secCount = 0;

    // 1 second timer
    timer->start(1000, this);
    checkNetwork();

    ui->dspeed->setAttribute( Qt::WA_TransparentForMouseEvents );
    ui->dspeed->setFocusPolicy( Qt::NoFocus );

    ui->uspeed->setAttribute( Qt::WA_TransparentForMouseEvents );
    ui->uspeed->setFocusPolicy( Qt::NoFocus );
}

wNetwork::~wNetwork()
{
    delete ui;
}

void wNetwork::closeEvent(QCloseEvent *event)
{
    saveUsageInfo(true);
    event->accept();
}

void wNetwork::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        if (secCount == 60) { // Save usage info after 60 seconds
            saveUsageInfo(false);
            secCount = 0;
        }
        secCount++;
        checkNetwork();
    }

    event->accept();
}

void wNetwork::checkNetwork()
{

    quint64 RecvBytes = im->getRXbytes();
    quint64 TrnsBytes = im->getTXbytes();

    quint64 RecvSpeed = nprevRXBytes ? RecvBytes - nprevRXBytes : 0;
    quint64 TrnsSpeed = nprevTXBytes ? TrnsBytes - nprevTXBytes : 0;

    nprevRXBytes = RecvBytes;
    nprevTXBytes = TrnsBytes;

    ui->dspeed->setText("↓" + PaperPrime::FileFunc::formatSize(RecvSpeed));
    ui->uspeed->setText("↑" + PaperPrime::FileFunc::formatSize(TrnsSpeed));

}

void wNetwork::saveUsageInfo(bool close)
{
    QDateTime datetime = QDateTime::currentDateTime();

    QString monthName = datetime.toString("MMMM");
    QStringList values;
    values << /*PaperPrime::FileFunc::formatSize*/QString("%1").arg(im->getRXbytes());
    values << /*PaperPrime::FileFunc::formatSize*/QString("%1").arg(im->getTXbytes());

    usageFile->setValue(monthName + "/" + currDT.toString("dd.MM.yyyy"), values);
    usageFile->setValue("LastChecked", datetime);
    usageFile->setValue("Close", close);
    usageFile->sync();
}

/* Name of the plugin */
QString networkPlugin::name()
{
    return "Network Speeds";
}

/* The plugin version */
QString networkPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *networkPlugin::widget(QWidget *parent)
{
    return new wNetwork(parent);
}
