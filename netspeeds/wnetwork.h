/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QTimer>
#include <QCloseEvent>
#include <QSettings>
#include <QDateTime>

#include <paperprime/cplugininterface.h>

class InfoManager;

namespace Ui {
    class wNetwork;
}

class wNetwork : public QWidget {
    Q_OBJECT

public:
    explicit wNetwork(QWidget *parent = nullptr);
    ~wNetwork();

private slots:
    void closeEvent(QCloseEvent *event) override;
    void timerEvent(QTimerEvent *event) override;

private:
    Ui::wNetwork *ui;
    QBasicTimer *timer;
    InfoManager *im;
    QSettings *usageFile;
    QDateTime currDT;
    int secCount;
    quint64 nprevRXBytes = 0, nprevTXBytes = 0;

    void checkNetwork();
    void saveUsageInfo(bool close);
};

class networkPlugin : public plugininterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID COREACTION_PLUGININTERFACE/*"com.coreaction.plugininterface"*/)
    Q_INTERFACES(plugininterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};

