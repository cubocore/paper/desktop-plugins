/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QDebug>
#include <QDoubleValidator>

#include "wcalculator.h"
#include "ui_wcalculator.h"


wCalculator::wCalculator(QWidget *parent) : QWidget(parent), ui(new Ui::wCalculator)
{
    ui->setupUi(this);

    ui->calcview->setValidator(new QDoubleValidator(0, 99999999, 99999999, this));
}

wCalculator::~wCalculator()
{
    delete ui;
}

void wCalculator::on_one_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "1");
}

void wCalculator::on_two_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "2");
}

void wCalculator::on_three_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "3");
}

void wCalculator::on_four_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "4");
}

void wCalculator::on_five_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "5");
}

void wCalculator::on_six_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "6");
}

void wCalculator::on_seven_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "7");
}

void wCalculator::on_eight_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "8");
}

void wCalculator::on_nine_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "9");
}

void wCalculator::on_zero_clicked()
{
    ui->calcview->setText(ui->calcview->text() + "0");
}

void wCalculator::on_dot_clicked()
{
    ui->calcview->setText(ui->calcview->text() + ".");
}

void wCalculator::on_equal_clicked()
{
    QString a, b ;
    a = ui->history->text();
    b = ui->calcview->text();
    varB = ui->calcview->text().toFloat();

    switch (z) {
        case 1 :
            result = varA + varB ;
            ui->history->setText(a + b);
            break;

        case 2 :
            result = varA - varB ;
            ui->history->setText(a + b);
            break;

        case 3 :
            result = varA * varB ;
            ui->history->setText(a + b);
            break;

        case 4 :
            result = varA / varB ;
            ui->history->setText(a + b);
            break;

        case 5 :
            varC = static_cast<int>(varA);
            varD = static_cast<int>(varB);// cast
            result = varC % varD ;
            ui->history->setText(a + b);
            break;
    }

    QString sss = QString::number(static_cast<double>(result));
    ui->calcview->setText(sss);
}

void wCalculator::on_clear_clicked()
{
    ui->calcview->setText("");
    ui->history->setText("");
}

void wCalculator::on_multiply_clicked()
{
    z = 3;
    varA = ui->calcview->text().toFloat();
    ui->history->setText(ui->calcview->text() + "x");
    ui->calcview->setText("");
}

void wCalculator::on_subtract_clicked()
{
    z = 2;
    varA = ui->calcview->text().toFloat();
    ui->history->setText(ui->calcview->text() + "-");
    ui->calcview->setText("");
}

void wCalculator::on_add_clicked()
{
    z = 1;
    varA = ui->calcview->text().toFloat();
    ui->history->setText(ui->calcview->text() + "+");
    ui->calcview->setText("");
}

void wCalculator::on_division_clicked()
{
    z = 4;
    varA = ui->calcview->text().toFloat();
    ui->history->setText(ui->calcview->text() + "/");
    ui->calcview->setText("");
}

/* Name of the plugin */
QString calculatorPlugin::name()
{
    return "Calculator";
}

/* The plugin version */
QString calculatorPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *calculatorPlugin::widget(QWidget *parent)
{
    return new wCalculator(parent);
}
