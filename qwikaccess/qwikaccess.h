/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QBasicTimer>
#include <paperprime/cplugininterface.h>

QT_BEGIN_NAMESPACE
namespace Ui {
    class qwikaccess;
}
QT_END_NAMESPACE

class qwikaccess : public QWidget {
    Q_OBJECT

public:
    qwikaccess(QWidget *parent = nullptr);
    ~qwikaccess();

protected:
    void timerEvent(QTimerEvent *tEvent);

private slots:
    void on_toolButton_nightmode_clicked(bool checked);
    void on_toolButton_performacemode_clicked(bool checked);
    void on_toolButton_batterysaver_clicked(bool checked);
    void on_toolButton_flashlight_clicked(bool checked);
    void on_toolButton_cameraoff_clicked(bool checked);
    void on_toolButton_touchpadoff_clicked(bool checked);
    void on_toolButton_touchscreenoff_clicked(bool checked);
    void on_toolButton_kbdbacklight_clicked(bool checked);
    void on_toolButton_displayoff_clicked();
    void check_status();
    void init();
    void on_toolButton_keyboardoff_clicked(bool checked);

private:
    Ui::qwikaccess *ui;
    QBasicTimer *timer;
};

class qwikaccessPlugin : public plugininterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID COREACTION_PLUGININTERFACE)
    Q_INTERFACES(plugininterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};
