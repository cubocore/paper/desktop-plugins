/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QProcess>
#include <QDebug>
#include <QMessageBox>

#include "qwikaccess.h"
#include "ui_qwikaccess.h"

qwikaccess::qwikaccess(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::qwikaccess),
      timer(new QBasicTimer)
{
    ui->setupUi(this);
    init();
}

qwikaccess::~qwikaccess()
{
    delete ui;
}

void qwikaccess::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == timer->timerId()) {
        //get_playing_media();
        check_status();
    }
}

void qwikaccess::init()
{
    // 1 second timer
    timer->start(1000, this);

    // initialization
    check_status();
}

void qwikaccess::check_status()
{
    QProcess proc;

    //performance mode status
    proc.start("cat", QStringList() << "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
    proc.waitForFinished();

    QString gov = proc.readAllStandardOutput();
    gov = gov.trimmed();

    //qDebug()<< gov;
    if (gov == "performance") {
        ui->toolButton_performacemode->setChecked(true);
    } else {
        ui->toolButton_performacemode->setChecked(false);
    }

    //batarysaver status
    if (gov == "powersave") {
        ui->toolButton_batterysaver->setChecked(true);
    } else {
        ui->toolButton_batterysaver->setChecked(false);
    }

    //flashlight status
    proc.start("cat", QStringList() << "/sys/class/leds/led:switch/brightness");
    proc.waitForFinished();

    QString flash = proc.readAllStandardOutput();
    flash = flash.trimmed();

    //qDebug()<< flash;
    if (flash == "1") {
        ui->toolButton_flashlight->setChecked(true);
    } else {
        ui->toolButton_flashlight->setChecked(false);
    }

    // nightmode status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-nightmode.sh");
    proc.waitForFinished();

    QString night = proc.readAllStandardOutput();
    night = night.trimmed();

    //qDebug()<< night;
    if (night == "enabled") {
        ui->toolButton_nightmode->setChecked(true);
    } else if (night == "disabled") {
        ui->toolButton_nightmode->setChecked(false);
    }

    // keyboard off status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-keyboard.sh");
    proc.waitForFinished();

    QString keyboard = proc.readAllStandardOutput();
    keyboard = keyboard.trimmed();

    //qDebug()<< keyboard;
    if (keyboard == "disabled") {
        ui->toolButton_keyboardoff->setChecked(true);
    } else {
        ui->toolButton_keyboardoff->setChecked(false);
    }

    // touchpad off status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-touchpad.sh");
    proc.waitForFinished();

    QString touchpad = proc.readAllStandardOutput();
    touchpad = touchpad.trimmed();

    //qDebug()<< touchpad;
    if (touchpad == "disabled") {
        ui->toolButton_touchpadoff->setChecked(true);
    } else {
        ui->toolButton_touchpadoff->setChecked(false);
    }

    // touchscreen off status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-touchscreen.sh");
    proc.waitForFinished();

    QString touchscreen = proc.readAllStandardOutput();
    touchscreen = touchscreen.trimmed();

    //qDebug()<< touchscreen;
    if (touchscreen == "disabled") {
        ui->toolButton_touchscreenoff->setChecked(true);
    } else {
        ui->toolButton_touchscreenoff->setChecked(false);
    }

    // camera off status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-camera.sh");
    proc.waitForFinished();

    QString camera = proc.readAllStandardOutput();
    camera = camera.trimmed();

    //qDebug()<< camera;
    if (camera == "disabled") {
        ui->toolButton_cameraoff->setChecked(true);
    }

    //else
    //ui->toolButton_cameraoff->setChecked(false);
    // kbd-backlight status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-backlight.sh");
    proc.waitForFinished();

    QString kbd = proc.readAllStandardOutput();
    kbd = kbd.trimmed();

    //qDebug()<< kbd;
    if (kbd == "enabled") {
        ui->toolButton_kbdbacklight->setChecked(true);
    } else {
        ui->toolButton_kbdbacklight->setChecked(false);
    }
}

void qwikaccess::on_toolButton_nightmode_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/nightmode-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/nightmode-off.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_performacemode_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/performance.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/powersave.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_batterysaver_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/powersave.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/performance.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_flashlight_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/flashlight-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/flashlight-off.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_cameraoff_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/camera-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/camera-off.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_touchpadoff_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/touchpad-off.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/touchpad-on.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_touchscreenoff_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/touchscreen-off.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/touchscreen-on.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_kbdbacklight_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/kbd-backlight-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/paperdesktop/scripts/kbd-backlight-off.sh");
        proc.waitForFinished();
    }
}

void qwikaccess::on_toolButton_displayoff_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Qwikaccess");
    msgBox.setText("Do you want to turn display off?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if (msgBox.exec() == QMessageBox::Yes) {
        // do something
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/dpms-off.sh");
        proc.waitForFinished();
    }//else {

    // do something else
    //}
}

void qwikaccess::on_toolButton_keyboardoff_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/keyboard-off.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/keyboard-on.sh");
        proc.waitForFinished();
    }
}
/* Name of the plugin */
QString qwikaccessPlugin::name()
{
    return "qwikaccess";
}

/* The plugin version */
QString qwikaccessPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *qwikaccessPlugin::widget(QWidget *parent)
{
    return new qwikaccess(parent);
}
