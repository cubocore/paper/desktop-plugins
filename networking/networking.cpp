/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QProcess>

#include "networking.h"
#include "ui_networking.h"

networking::networking(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::networking),
      timer(new QBasicTimer)
{
    ui->setupUi(this);
    init();
}

networking::~networking()
{
    delete ui;
}

void networking::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == timer->timerId()) {
        check_status();
    }
}

void networking::init()
{
    // 1 second timer
    timer->start(1000, this);

    // initialization
    check_status();
}

void networking::check_status()
{
    QProcess proc;

    //wifi status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-wifi.sh");
    proc.waitForFinished();

    QString wifi = proc.readAllStandardOutput();
    wifi = wifi.trimmed();

    //qDebug()<< wifi;
    //ui->wifi->setText(w);
    if (wifi == "enabled") {
        ui->toolButton_wifi->setChecked(true);
    } else {
        ui->toolButton_wifi->setChecked(false);
    }

    // bluetooth status
    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-bt.sh");
    proc.waitForFinished();

    QString bt = proc.readAllStandardOutput();
    bt = bt.trimmed();

    //qDebug()<< bt;
    if (bt == "enabled") {
        ui->toolButton_bluetooth->setChecked(true);
    } else {
        ui->toolButton_bluetooth->setChecked(false);
    }

    //airplanemode status

    proc.start("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/check-airplane.sh");
    proc.waitForFinished();

    QString airp = proc.readAllStandardOutput();
    airp = airp.trimmed();

    if (airp == "enabled") {
        ui->toolButton_airplanemode->setChecked(true);
    } else {
        ui->toolButton_airplanemode->setChecked(false);
    }

    //gps
    proc.start("systemctl", QStringList() << "is-enabled" << "geoclue");
    proc.waitForFinished();

    QString gps = proc.readAllStandardOutput();
    gps = gps.trimmed();

    //qDebug()<< gps;
    if (gps == "static") {
        ui->toolButton_gps->setChecked(true);
    } else if (gps == "active") {
        ui->toolButton_gps->setChecked(true);
    } else {
        ui->toolButton_gps->setChecked(false);
    }

}

void networking::on_toolButton_wifi_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/wifi-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/wifi-off.sh");
        proc.waitForFinished();
    }
}

void networking::on_toolButton_quickhotspot_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/hotspot-on.sh");
        proc.waitForFinished();
        ui->toolButton_quickhotspot->setText("QuickHotspot \npass123456789");
//        ui->hotspot->setAlignment(Qt::AlignCenter);
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/hotspot-off.sh");
        proc.waitForFinished();
        ui->toolButton_quickhotspot->setText("Hotspot");
    }
}

void networking::on_toolButton_bluetooth_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/bt-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/bt-off.sh");
        proc.waitForFinished();
    }
}

void networking::on_toolButton_airplanemode_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/airplane-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/airplane-off.sh");
        proc.waitForFinished();
    }
}

void networking::on_toolButton_gps_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/qwikaccess/scripts/gps-on.sh");
        proc.waitForFinished();
    } else { //off
        QProcess proc;
        proc.start("pkexec", QStringList() << "/bin/sh" << "/usr/share/qwikaccess/scripts/gps-off.sh");
        proc.waitForFinished();
    }
}

/* Name of the plugin */
QString networkingPlugin::name()
{
    return "Networking Switch";
}

/* The plugin version */
QString networkingPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *networkingPlugin::widget(QWidget *parent)
{
    return new networking(parent);
}
