TEMPLATE = subdirs

# Directories
SUBDIRS += \
    backlight   \
    battery \
    calculator  \
    calendar    \
    media   \
    mpactlqt    \
    netspeeds   \
    networking  \
    notes   \
    pactlqt \
    playerctlqt \
    qwikaccess  \
    rotation    \
    search  \
    system  \
    weather
