/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QDir>
#include <QTimer>
#include <QTime>

#include <csys/battery.h>

#include "wbattery.h"

QString getTimeString(QTime t)
{
	QString time;
    int hh = t.hour();
    int mm = t.minute();

    if (hh > 0) {
        time += QString("%1 hour%2").arg(hh).arg(hh > 1 ? "s" : "");
	}

    if (mm > 0) {
        if (hh > 0) {
			time += ", ";
        }

        time += QString("%1 minute%2").arg(mm).arg(mm > 1 ? "s" : "");
	}

	return time;
};

wBattery::wBattery(QWidget *parent) : QWidget(parent)
{
	batman = BatteryManager::instance();

	QTimer *timer = new QTimer();
    timer->start(1000);

    connect(timer, &QTimer::timeout, this, &wBattery::updateBatteries);
}

wBattery::~wBattery()
{
	batman->deleteLater();
}

void wBattery::updateBatteries()
{
    qDeleteAll(children());

    QVBoxLayout *batLyt = new QVBoxLayout();

    QLabel *lbl = new QLabel("BATTERIES");
    lbl->setFont(QFont(font().family(), 11));
    lbl->setAlignment(Qt::AlignLeft);

    batLyt->addWidget(lbl);

    if (not batman->batteries().count()) {
        QLabel *noBatLbl = new QLabel("No batteries detected.");
        noBatLbl->setAlignment(Qt::AlignCenter);
        batLyt->addWidget(noBatLbl);
        setLayout(batLyt);

		return;
	}

    Q_FOREACH (Battery bat, batman->batteries()) {
		QString name = bat.name();
        int level = bat.value("Percentage").toInt();
        QString time;

        bool fullCharge = false;
        QVariant val;

        switch (bat.value("State").toUInt()) {
			case 1: {
                val = bat.value("TimeToFull");
				time = "Charged in: ";
				break;
			}

			case 2: {
                val = bat.value("TimeToEmpty");
				time = "Discharged in: ";
				break;
			}

			default: {
				time = "Fully Charged";
                fullCharge = true;
				break;
			}
		}

        if (!fullCharge) {
            QTime tmp = QDateTime(QDate::currentDate(), QTime(0, 0, 0, 0)).addSecs(val.toLongLong()).time();

            if (val.toLongLong() > 0) {
                time += getTimeString(tmp);
            }
        }

		QProgressBar *pb = new QProgressBar();
        pb->setRange(0, 100);
        pb->setValue(level);

		QLabel *timeLbl = new QLabel();
        timeLbl->setAlignment(Qt::AlignCenter);
        timeLbl->setText(time);

		QVBoxLayout *boxLyt = new QVBoxLayout();
        boxLyt->addWidget(pb);
        boxLyt->addWidget(timeLbl);

        QGroupBox *gb = new QGroupBox(name, this);
        gb->setLayout(boxLyt);

        batLyt->addWidget(gb);
	}

    setLayout(batLyt);
}

/* Name of the plugin */
QString BatteryPlugin::name()
{
    return "Battery";
}

/* The plugin version */
QString BatteryPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *BatteryPlugin::widget(QWidget *parent)
{
    return new wBattery(parent);
}
