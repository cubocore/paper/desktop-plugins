/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QFile>
#include <QDir>
#include <QVBoxLayout>
#include <QLabel>
#include <QProcess>

#include <paperprime/paperprime.h>

#include "backlightplugin.hpp"


BacklightDevice::BacklightDevice(QString path)
{
    devPath = QString(path);

    QFile curFile(devPath + "/max_brightness");

	maxBrightness = 255.0;

    if (curFile.open(QIODevice::ReadOnly)) {
        maxBrightness = QString::fromLocal8Bit(curFile.readAll()).simplified().toDouble();
    }

	curFile.close();
}

QString BacklightDevice::name()
{
    return QFileInfo(devPath).fileName();
}

qreal BacklightDevice::currentBrightness()
{
	/* Brightness scaled to 1000 */
    QFile curFile(devPath + "/brightness");

    if (not curFile.open(QIODevice::ReadOnly)) {
		return -1;
    }

    qreal curBrightness = QString::fromLocal8Bit(curFile.readAll()).simplified().toDouble();
	curFile.close();

    return curBrightness * 1000.0 / maxBrightness;
}

bool BacklightDevice::setBrightness(qreal value)
{
    QString brightnessFile( devPath + "/brightness" );

	if ( abs( currentBrightness() - value * 1000 ) < 1 )
		return true;

    qreal brightness = value * maxBrightness;

    if ( not brightness )
		brightness++;

	QProcess proc;
	proc.start( "pkexec", { "/usr/libexec/paperdesktop/corepkit", "--brightness", QString::number( ( int )brightness ), brightnessFile } );
	proc.waitForFinished();

	return true;
}

BacklightWidget::BacklightWidget(QWidget *parent) : QWidget(parent)
{
    Q_FOREACH (QFileInfo blInfo, QDir("/sys/class/backlight/").entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries)) {
        backlights << BacklightDevice(QFileInfo("/sys/class/backlight/" + blInfo.fileName()).symLinkTarget());
    }

	/* For devices which have led based backlighting */
	if ( QFile::exists( "/sys/class/leds/lcd-backlight" ) )
		backlights << BacklightDevice("/sys/class/leds/lcd-backlight/");

    maxBrightness = 255;            // So that the screen never goes to zero
    curBrightness = 1;              // So that the screen never goes to zero

    QLabel *lbl = new QLabel("BRIGHTNESS");
    lbl->setFont(QFont(font().family(), 11));
    lbl->setAlignment(Qt::AlignLeft);

    Q_FOREACH (BacklightDevice dev, backlights) {
        QSlider *slider = new QSlider(Qt::Horizontal, this);
        slider->setRange(1, 1000);
		connect(slider, &QSlider::sliderReleased, [=]() {
			changeBacklight(slider->value());
		});
//		connect(slider, SIGNAL(sliderReleased()), this, SLOT(changeBacklight(int)));
		connect(slider, SIGNAL(valueChanged(int)), this, SLOT(changeBacklight(int)));

		sliders << slider;
	}

	setCurrent();

    timer.start(1000, this);                     // 1 sec timer. Checks if any other process has changed the brightness behind the scenes.

	QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setAlignment(Qt::AlignLeft);
    lyt->addWidget(lbl);

	/* Only one device, no name needed */
    if (backlights.count() == 1) {

        lyt->addWidget(sliders[ 0 ]);
	}

	/* Multiple devices, name them */
	else {
        for (int i = 0; i < backlights.count(); i++) {
            lyt->addWidget(new QLabel(backlights[ i ].name()));
            lyt->addWidget(sliders[ i ]);
		}
	}

    setLayout(lyt);
}

void BacklightWidget::setCurrent()
{
    for (int i = 0; i < backlights.count(); i++) {
        if (sliders[i]->isSliderDown()) {
			continue;
        }

        qreal brightness = backlights[i].currentBrightness();

        if (brightness == -1) {
            PaperPrime::InfoFunc::messageEngine("CoreAction",
                                            "coreaction",
                                            "Brightness Add-on",
                                            "Cannot get the current brightness of the screen. Default value taken as 100.",
                                            this);
            sliders[i]->setValue(400);

			continue;
		}

        sliders[i]->setValue(brightness);
	}
}

void BacklightWidget::changeBacklight(int value)
{
	/* Get the index of the slider which was moved */
    QSlider *slider = qobject_cast<QSlider *>(sender());

	int sliderIndex = -1;

    if (slider) {
        for (int i = 0; i < sliders.count(); i++) {
            if (slider == sliders[ i ]) {
				sliderIndex = i;
				break;
			}
		}

		if (slider->isSliderDown()) {
			changedValue = value; // Wait for the moving to be stopped
			return;
		} else {
			if (changedValue >= 0) {
				slider->setValue(changedValue);
				changedValue = -1;
			}
		}

		backlights[ sliderIndex ].setBrightness(1.0 * slider->value() / 1000);
	}
}

void BacklightWidget::timerEvent(QTimerEvent *tEvent)
{
    if (tEvent->timerId() == timer.timerId()) {
		/* Set the actual position of the slider */

		setCurrent();
	}

    QWidget::timerEvent(tEvent);
	tEvent->accept();
}

/* Name of the plugin */
QString BacklightPlugin::name()
{
	return "Backlight";
}

/* The plugin version */
QString BacklightPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *BacklightPlugin::widget(QWidget *parent)
{
    return new BacklightWidget(parent);
}
