/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QTimerEvent>
#include <QBasicTimer>
#include <QSlider>
#include <QtPlugin>

#include <paperprime/cvariables.h>
#include <paperprime/cplugininterface.h>


class BacklightDevice {

public:
    BacklightDevice(QString path);

    QString name();

    qreal currentBrightness();
    bool setBrightness(qreal value);

private:
    QString devPath;
    qreal maxBrightness;
};

typedef QList<BacklightDevice> BacklightDevices;

class BacklightWidget : public QWidget {

    Q_OBJECT

public:
    BacklightWidget(QWidget *parent);

private:
    QList<QSlider *> sliders;
    BacklightDevices backlights;

    qreal maxBrightness, curBrightness;

	QBasicTimer timer;
	int changedValue = -1;

    void setCurrent();

private Q_SLOTS:
    void changeBacklight(int);

protected:
    void timerEvent(QTimerEvent *);
};

class BacklightPlugin : public plugininterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID COREACTION_PLUGININTERFACE)
    Q_INTERFACES(plugininterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);

};
