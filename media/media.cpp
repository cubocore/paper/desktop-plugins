/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QProcess>

#include "media.h"
#include "ui_media.h"

media::media(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::media)
{
    ui->setupUi(this);
}

media::~media()
{
    delete ui;
}

void media::on_toolButton_camera_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/camera.sh");
    proc.waitForFinished();
}

void media::on_toolButton_audiorecorder_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/audio-recorder.sh");
        proc.waitForFinished();
        ui->toolButton_audiorecorder->setText("Stop Recording");
//        ui->audiorecorder->setAlignment(Qt::AlignCenter);
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/stop-recorder.sh");
        proc.waitForFinished();
        ui->toolButton_audiorecorder->setText("Audio Recorder");
//        ui->audiorecorder->setAlignment(Qt::AlignCenter);
    }
}

void media::on_toolButton_screenshot_clicked()
{
    QProcess proc;
    proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/screenshot.sh");
    proc.waitForFinished();
}

void media::on_toolButton_screenrecorder_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/screen-recorder.sh");
        proc.waitForFinished();
        ui->toolButton_screenrecorder->setText("Stop Recording");
//        ui->screenrecorder->setAlignment(Qt::AlignCenter);
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/stop-recorder.sh");
        proc.waitForFinished();
        ui->toolButton_screenrecorder->setText("Screen Recorder");
//        ui->screenrecorder->setAlignment(Qt::AlignCenter);
    }
}

void media::on_toolButton_screencam_clicked(bool checked)
{
    if (checked) { //on
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/screencam-recorder.sh");
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/toggle_always_above.sh");
        proc.waitForFinished();
        ui->toolButton_screencam->setText("Stop Recording");
//        ui->screencam->setAlignment(Qt::AlignCenter);
    } else { //off
        QProcess proc;
        proc.startDetached("/bin/sh", QStringList() << "/usr/share/paperdesktop/scripts/stop-recorder.sh");
        proc.waitForFinished();
        ui->toolButton_screencam->setText("Screencam Recorder");
//        ui->screencam->setAlignment(Qt::AlignCenter);
    }
}

/* Name of the plugin */
QString mediaPlugin::name()
{
    return "Media";
}

/* The plugin version */
QString mediaPlugin::version()
{
    return QString(VERSION_TEXT);
}

/* The Widget hooks for menus/toolbars */
QWidget *mediaPlugin::widget(QWidget *parent)
{
    return new media(parent);
}
