TEMPLATE = lib
TARGET = media

QT += widgets

VERSION = 1.0.0

# Library section
unix:!macx: LIBS += -lpaperprime

# thread - Enable threading support
# silent - Do not print the compilation syntax
CONFIG  += thread silent plugin

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Warn about deprecated features
DEFINES += QT_DEPRECATED_WARNINGS
# Disable all deprecated features before Qt 5.15
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051500

# Build location
BUILD_PREFIX = $$(CA_BUILD_DIR)
isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-plugins/
OBJECTS_DIR   = $$BUILD_PREFIX/obj-plugins/
RCC_DIR       = $$BUILD_PREFIX/qrc-plugins/
UI_DIR        = $$BUILD_PREFIX/uic-plugins/

# C++17 Support for Qt5
QMAKE_CXXFLAGS += -std=c++17

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        RESOURCE_PATH = $$PREFIX/share/paperdesktop/

        DEFINES += RESOURCE_PATH_TEXT=\"\\\"$${RESOURCE_PATH}\\\"\"
        DEFINES += VERSION_TEXT=\"\\\"$${VERSION}\\\"\"

        scripts.files = scripts
        scripts.path  = $$RESOURCE_PATH

        config.path = $$PREFIX/share/paperdesktop/metadata/
        config.files = media.ini

        INSTALLS += target config scripts
        target.path = $$PREFIX/lib/paperdesktop/plugins
}


SOURCES += \
    media.cpp

HEADERS += \
    media.h

FORMS += \
    media.ui
