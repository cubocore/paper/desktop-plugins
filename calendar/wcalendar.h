/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QMap>

#include <paperprime/cplugininterface.h>

namespace Ui {
    class wCalendar;
}

class wCalendar : public QWidget {
    Q_OBJECT

public:
    enum Type {
        Reminder,
        Holiday
    };

    struct CalendarEvent {
        Type type;
        QString description;
    };

    typedef QMap < QDate, QList < CalendarEvent > > CalendarData;

    explicit wCalendar(QWidget *parent = nullptr);
    ~wCalendar();

private slots:
    void on_back_clicked();
    void on_calendar_clicked(const QDate &date);
    void on_calendar_currentPageChanged(int year, int month);

private:
    Ui::wCalendar *ui;
    CalendarData m_events;
    QString configPath;
    QString m_internationalHEventFile;
    QString m_countryHEventFile;
    bool reminderList;

    void setupReminders();
    void setupCalendarEvents(QString eventFile);
};

class calendarPlugin : public plugininterface {

    Q_OBJECT

    Q_PLUGIN_METADATA(IID COREACTION_PLUGININTERFACE/*"com.coreaction.plugininterface"*/)
    Q_INTERFACES(plugininterface)

public:
    /* Name of the plugin */
    QString name();

    /* The plugin version */
    QString version();

    /* The Widget */
    QWidget *widget(QWidget *);
};
