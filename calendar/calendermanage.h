/*
    *
    * This file is a part of CoreAction.
    * A side bar for showing widgets for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#ifndef CALENDERMANAGE_H
#define CALENDERMANAGE_H

#include <QCalendarWidget>
#include <QDate>

class calendermanage: public QCalendarWidget
{
    Q_OBJECT

public:
    calendermanage(QWidget *parent = nullptr);
    ~calendermanage();

    // type == 1 is holiday; type == 0 is reminder
    void addEvent(QDate date, int type);

    void clearEvents();

protected:
    virtual void paintCell(QPainter *painter, const QRect &rect, const QDate &date) const;

private:
    QList<QDate> reminders;
    QList<QDate> holidays;

};

#endif // CALENDERMANAGE_H
